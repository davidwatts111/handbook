---
title: "Development Department Career Framework: Intern"
---

## Development Department Competencies: Intern

{{% include "includes/engineering/dev-career-matrix-nav.md" %}}

**Interns at GitLab are expected to exhibit the following competencies:**

- [Intern Technical Competencies](#intern-technical-competencies)
- [Intern Values Alignment](#intern-values-alignment)

---

### Intern Leadership Competencies

{{% include "includes/engineering/intern-leadership-competency.md" %}}
{{% include "includes/engineering/development-intern-leadership-competency.md" %}}
  
### Intern Technical Competencies

{{% include "includes/engineering/intern-technical-competency.md" %}}
{{% include "includes/engineering/development-intern-technical-competency.md" %}}

### Intern Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-intern-values-competency.md" %}}

