---
title: "Enablement & SaaS Platforms Quality Engineering team"
description: "Quality Engineering team for the Enablement & SaaS Platforms  Sections"
---

## Common Links

| **GitLab Team Handle** | [`@gl-quality/enablement-qe`](https://gitlab.com/gl-quality/enablement-qe) |
| **Slack Channel** | [#g_qe_enablement_platform](https://gitlab.slack.com/archives/CTP7N0PM4) |
| **Team Boards** | [Team Board](https://gitlab.com/groups/gitlab-org/-/boards/978354) |
| **Issue Tracker** | [quality/team-tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/) |
| **Quality Engineering Enablement Projects** | [Tools Page](https://about.gitlab.com/handbook/engineering/infrastructure/test-platform/self-managed-excellence/) |

Engineers in this team are embedded in the [Enablement section] and [SaaS Platforms section].

## Team members

Quality Engineering Manager: Kassandra Svoboda

[Enablement section]

| S.No 	| Stage       	| Group                     | SET Counterpart                                                        |
|------	|---------------|---------------------------|------------------------------------------------------------------------|
| 1    	| [Systems]     | [Distribution]            | Grant Young                                     |
| 2    	| [Systems]     | [Distribution:Build]    | Nailia Iskhakova                                 |
| 3    	| [Systems]     | [Distribution:Deploy]   | Vishal Patel                                     |
| 4   	| [Systems]     | [Geo]                     | Nick Westbury                                                        	 |
| 5   	| [Systems]     | [Gitaly]                  | John McDonnell                                                         |
| 6   	| [Data Stores] | [Global Search]           | TBH                                                           |
| 7  	| [Data Stores] | [Database]                | TBH                                                                    |
| 8   	| [Data Stores] | [Tenant Scale]                    | Andy Hohenner (stable counterpart for [US Public Sector Services] product group) is currently supporting                                                                    |

[SaaS Platforms section]

| S.No 	| Stage           	| Group                          | SET Counterpart                                                         |
|------	|-------------------|--------------------------------|-------------------------------------------------------------------------|
| 1    	| [SaaS Platforms]  | [GitLab Dedicated]             | Brittany Wilkerson                                                                     |
| 2    	| [SaaS Platforms]  | [Switchboard]                  | Brittany Wilkerson                                                                     |
| 3    	| [SaaS Platforms]  | [US Public Sector Services]    | Andy Hohenner <br/> Jim Baumgardner                                                                      |

## OKRs

Every quarter, the team commits to [Objectives and Key Results (OKRs)](/company/okrs/). The below shows current quarter OKRs and is updated regularly as the quarter progresses.

- [Overview](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3773) of our current Enablement & SaaS Platforms Quality Engineering OKR.

## Quality Performance Indicators for Enablement Section

{{< sisense-with-filters dashboard="736012" Development_Section="enablement" >}}

## Projects

- [GitLab Performance Tool]
- [GitLab Browser Performance Tool]
- [GitLab Environment Toolkit]
- [Performance Test Data]
- [Performance Docker Images]
- [Zero Downtime Testing Tool]
- [Reference Architectures]

## Quality Engineering process across groups

Overall we follow the same process as [defined](https://about.gitlab.com/handbook/engineering/infrastructure/test-platform/#how-we-work) in our Quality Engineering handbook across all groups in Enablement
except for a few exceptions curated to fit the needs of specific groups.

- [Quality Engineering in Distribution group](/handbook/engineering/infrastructure/test-platform/enablement-saas-platforms-qe-team/distribution/index.html)

[Core Platform section]: /handbook/product/categories/#core-platform-section
[Systems]: /handbook/product/categories/#systems-stage
[Data Stores]: /handbook/product/categories/#data-stores-stage
[Distribution]: /handbook/product/categories/#distributionbuild-group
[Distribution:Build]: /handbook/product/categories/#distributionbuild-group
[Distribution:Deploy]: /handbook/product/categories/#distributiondeploy-group
[Geo]: /handbook/product/categories/#geo-group
[Gitaly]: /handbook/product/categories/#gitalycluster-group
[Application Performance]: /handbook/product/categories/#application-performance-group
[Global Search]: /handbook/product/categories/#global-search-group
[Database]: /handbook/product/categories/#database-group
[Tenant Scale]: /handbook/product/categories/#tenant-scale-group
[SaaS Platforms section]: /handbook/product/categories/#saas-platforms-section
[SaaS Platforms]: /handbook/product/categories/#saas-platforms-stage
[Delivery]: /handbook/product/categories/#delivery-group
[Scalability]: /handbook/product/categories/#scalability-group
[GitLab Dedicated]: /handbook/product/categories/#gitlab-dedicated-group
[Switchboard]: /handbook/product/categories/#switchboard-group
[US Public Sector Services]: /handbook/product/categories/#us-public-sector-services-group
[GitLab Performance Tool]: https://gitlab.com/gitlab-org/quality/performance
[GitLab Browser Performance Tool]: https://gitlab.com/gitlab-org/quality/performance-sitespeed
[GitLab Environment Toolkit]: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
[Performance Test Data]: https://gitlab.com/gitlab-org/quality/performance-data
[Performance Docker Images]: https://gitlab.com/gitlab-org/quality/performance-images
[Zero Downtime Testing Tool]: https://gitlab.com/gitlab-org/quality/zero-downtime-testing-tool
[Reference Architectures]: https://gitlab.com/gitlab-org/quality/reference-architectures
