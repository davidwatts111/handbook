---
aliases: /handbook/engineering/infrastructure/production/architecture/disaster-recovery.html
title: "Disaster Recovery Architecture"
---


This page has moved to [a single internal handbook page](https://internal.gitlab.com/handbook/engineering/gitlab-com-disaster-recovery)
